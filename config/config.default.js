/* eslint valid-jsdoc: "off" */

'use strict';
const path = require("path");
/**
 * @param {Egg.EggAppInfo} appInfo app info
 */
module.exports = appInfo => {
  /**
   * built-in config
   * @type {Egg.EggAppConfig}
   **/
  const config = exports = {};

  // use for cookie sign key, should change to your own and keep security
  config.keys = appInfo.name + '_1589962467450_5016';

  // add your middleware config here
  config.middleware = [];

  exports.mysql = {
    // 单数据库信息配置
    client: {
      // host
      host: '127.0.0.1',
      // 端口号
      port: '3306',
      // 用户名
      user: 'magic',
      // 密码
      password: 'CiiSMaw3a5Tn47LS',
      // 数据库名
      database: 'magic',
    },
    // 是否加载到 app 上，默认开启
    app: true,
    // 是否加载到 agent 上，默认关闭
    agent: false,
  };
  config.cluster = {
    listen: {
      path: '',
      port: 3560,
      hostname: '0.0.0.0',
    }
  };
  // 靜態目錄及緩存設置
  config.static = {
    prefix: '/static', //靜態化URL  我這裏默認網站根目錄（項目需要）
    dir: path.join(appInfo.baseDir, 'app/public'), // 靜態文件夾地址 可以設置多個 可以簡寫為 ：['app/public','app/public1']
    dynamic: true, //是否緩存靜態資源
    preload: false, //啓動項目開啓緩存
    // maxAge: 31536000,
    maxAge: -1, //緩存時間 開發建議設0 跳坑
    buffer: false, //是否緩存到内存 默認prod 緩存
  };
  // add your user config here
  const userConfig = {
    // myAppName: 'egg',
  };
  // add your config here
  config.middleware = [];
  //多出来的配置==========
  config.security = {
    csrf: {
      enable: false,
      ignoreJSON: true
    },
    // domainWhiteList: ['http://localhost:3550' , "http://106.12.76.26:3551"]
  };
  config.cors = {
    origin:'*',
    allowMethods: 'GET,HEAD,PUT,POST,DELETE,PATCH'
  };
  return {
    ...config,
    ...userConfig,
  };
};
