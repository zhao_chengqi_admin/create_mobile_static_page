const Path = require('path')
const VueLoaderPlugin = require('vue-loader/lib/plugin');  // webpack 4版本之后加的，之前的版本不需要这个
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

let config = {
    entry: Path.resolve(__dirname, './app/web/src/main.js'), // 以join拼接path的形式配置绝对路径,相对路径打包后找不到会报错
    output: {
        filename: '[name].[hash:8].js',
        // path: Path.join(__dirname, './app/public/web'),
        // publicPath: '/static/web'
        path: Path.join(__dirname, './app/view'),
        publicPath: '/'
    },
    resolve: {
        alias: {
            '@': Path.resolve(__dirname, './app/web/src'), //设置@为src目录的别名
        }
    },
    module: {
        rules: [{
            test: /\.vue$/,
            loader: 'vue-loader'
        }, {
            test: /\.js$/,
            use: {
                loader: "babel-loader",
                options: {
                    presets: [
                        
                        ["@vue/babel-preset-jsx"]
                    ],
                    plugins: [
                        ["jsx-v-model"],
                        ['@babel/plugin-proposal-decorators', { legacy: true }],
                        ['@babel/plugin-proposal-class-properties', { loose: true }],
                        //   ["@vue/babel-plugin-transform-vue-jsx"]
                    ]
                }
            }
        }, {
            test: /\.less$/,
            use: [
                'style-loader',
                'css-loader',
                'postcss-loader',
                'less-loader',
            ],
            exclude: /node_modules/
        }, {
            test: /\.css$/,
            use: [
                'style-loader',
                'css-loader',
                'postcss-loader'
            ],
            // exclude: /node_modules/
        }, {
            test: /\.(png|jpg|jpeg|gif)$/,
            use: {
                loader: 'url-loader',
                options: {
                    limit: 1024, // 判断图片的大小   如果小于1024就会转换成base64
                    name: 'img/[name].[hash:7].[ext]' // 输出图片的名字  ext是扩展名
                }
            }
        }, {
            test: /\.(eot|svg|ttf|woff|woff2)(\?\S*)?$/,
            loader: 'file-loader'
        }]
    },
    devServer: { // 配置 服务器 信息 
        contentBase: Path.join(__dirname, './app/view'), // 托管的目录
        // contentBase:false,
        port: 3550, // 指定端口号
        open: false, // 自动打开浏览器
        historyApiFallback: {
            // HTML5 history模式
            rewrites: [{ from: /.*/, to: "/index.html" }]
        }
        //...
    },
    plugins: [
        new CleanWebpackPlugin(),
        new VueLoaderPlugin(),
        new HtmlWebpackPlugin({
            filename: Path.join(__dirname, './app/view/index.html'),
            template: Path.join(__dirname, './app/web/index.html') //以当前目录下的index.html文件为模板生成dist/index.html文件
        })
    ]
};

module.exports = config;