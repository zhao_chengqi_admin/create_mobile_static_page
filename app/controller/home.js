'use strict';

const Controller = require('egg').Controller;
const fs = require('fs');

class HomeController extends Controller {
  async showDom() {
    const { ctx } = this;
    console.log(ctx.headers.host)
    // try{
      // 页面路由
      let pageName =  ctx.query.pageName; 
      
      ctx.body = await ctx.service.page.get_page_detail(pageName);
    // }catch(e){
    //   ctx.body = "404" ;
    // }
  }
  async index() {
    this.ctx.body = await fs.readFileSync(__dirname + '/../view/index.html').toString();
  }
}

module.exports = HomeController;
