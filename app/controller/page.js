'use strict';

const Controller = require('egg').Controller;
const fs = require('fs');
const { initDomNode, insertNode, render } = require("../utils/common")
const uuid  = require("../utils/uuid")
class PageController extends Controller {
    async save() {
        const { ctx, app } = this;
        const { oldHtml, html, javascript, pageName } = ctx.request.body
        // console.log(JSON.stringify(html))
        // console.log(JSON.stringify(javascript))
        // console.log(JSON.stringify(pageName))
        // javaScriptOption
        // 保存脚本执行所需要的参数
        await this.app.mysql.query(`update magic_page set javaScriptOption='${JSON.stringify(javascript)}' where pageName='${pageName}'`);
        // 先获取父节点 为0 的id
        const page = await this.app.mysql.query(`select * from magic_page_view where view_name='${pageName}' and pid=0`);
        await this.app.mysql.query(`delete from magic_page_view where view_name='${pageName}' and pid<>0`);
        const pid = page[0].id
        const conn = await app.mysql.beginTransaction(); // 初始化事务 
        try {
            await ctx.service.page.page_save(oldHtml, pid, pageName);
            await ctx.service.page.page_save(html, pid, pageName);
            await conn.commit(); // 提交事务
            ctx.body = "200";
        } catch (err) {
            // error, rollback
            await conn.rollback(); // 一定记得捕获异常后回滚事务！！
            throw err;
        }

    }

    async list() {
        const { ctx, app } = this;
        const { page = 1 } = ctx.request.body
        const result = await this.app.mysql.query(`select * from magic_page limit ${(page-1)*8},8`);
        const conut = await this.app.mysql.query(`select count(*) as count from magic_page`);
        for (let item of result) item.dom = await this.ctx.service.page.get_page_detail(item.pageName);
        // result.map(item=>{
        //     console.log(item)
        //     
        // })
        // console.log(conut)
        ctx.body = {result , count : conut[0].count }
    }

    async savePageName() {
        const { ctx, app } = this;
        const { id, name } = ctx.request.body
        await this.app.mysql.query(`update magic_page set pageTitle='${name}' where id=${id}`);
        ctx.body = "ok"
    }

    async createPage(){
        const { ctx, app } = this;
        const { pagename } = ctx.request.body
        let pageName = uuid.uuid(10)
        let result = await this.app.mysql.query(`select * from magic_page where pageName='${pageName}'`) 
        while(result.length){
            pageName = uuid.uuid(10)
            result = await this.app.mysql.query(`select * from magic_page where pageName='${pageName}'`)
        }

        await this.app.mysql.insert('magic_page', {
            pageName ,
            pageTitle : pagename
        })
        await this.app.mysql.insert('magic_page_view', {
            pid : 0,
            magic_page_components_name : 'html',
            view_name : pageName
        });
        ctx.body = "ok"
    }

}

module.exports = PageController;
