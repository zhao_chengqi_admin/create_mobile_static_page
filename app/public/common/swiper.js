class swiper {
    constructor(options) {

        // swiper容器 
        this.el = document.getElementById(options.el); // swiper容器
        if(!this.el) return console.error("null");
        // swiper容器 滚动内容主体
        this.swiperEl = document.getElementById(options.swiperEl);
        // swiper 每一项内容
        this.swiperItemEl = document.getElementsByClassName(options.swiperItemEl);
        // swiper 指示器按钮 indicatorEl
        this.indicatorEl = options.indicatorEl && document.getElementsByClassName(options.indicatorEl);
        // 指示器当前样式
        this.indicatorActiveColor = options.indicatorActiveColor;
        this.indicatorColor = options.indicatorColor;
        // 自动轮播间隔，单位为 ms 
        this.autoplay = options.autoplay || 3000;
        // 动画时长，单位为 ms
        this.duration = options.duration || 500;
        // 是否为纵向滚动
        this.vertical = options.vertical || false;
        // 当前索引值
        this.swipeIndex = 1;
        this.loopSTO = "";
        this.touchCoordinate = {};
        this.initElement();

        this.addEvent(this.swiperEl, 'touchstart', this.touchstart.bind(this));
        this.addEvent(this.swiperEl, 'touchmove', this.touchmove.bind(this));
        this.addEvent(this.swiperEl, 'touchend', this.touchend.bind(this));

        this.startloop();

    }
    initElement() {
        // 设置滚动容器 dom节点的 style 
        this.swiperElStyle = {
            // transition: `transform ${this.duration}ms`,
            transition: `none`,
        }
        this.setSwiperElStyleTransform();
        this.setSwiperElStyle();
        this.setindicatorActiveClass();

        // this.indicatorActiveClass
        // let classArr =  document.getElementsByClassName("temp");
        // classArr[0].classList.remove("red");
        // DOM.classList.add("类名")

    }
    // 设置指示器选中样式
    setindicatorActiveClass() {
        // 指示器按钮
        if (this.indicatorEl) {
            for (let item of this.indicatorEl)
                item.style.background = this.indicatorColor 
            if (this.swipeIndex >= 1 && this.swipeIndex < this.swiperItemEl.length - 1)
                this.indicatorEl[this.swipeIndex - 1].style.background = (this.indicatorActiveColor)
            if (this.swipeIndex == (this.swiperItemEl.length - 1))
                this.indicatorEl[0].style.background = (this.indicatorActiveColor)
        }
        // if (this.indicatorEl) {
        //     for (let item of this.indicatorEl)
        //         item.classList.remove(this.indicatorActiveClass);
        //     if (this.swipeIndex >= 1 && this.swipeIndex < this.swiperItemEl.length - 1)
        //         this.indicatorEl[this.swipeIndex - 1].classList.add(this.indicatorActiveClass)
        //     if (this.swipeIndex == (this.swiperItemEl.length - 1))
        //         this.indicatorEl[0].classList.add(this.indicatorActiveClass)
        // }
    }
    // 设置滚动样式
    setSwiperElStyleTransform() {
        if (this.vertical) this.swiperElStyle.transform = `translate3d(0, -${this.swipeIndex * this.el.offsetHeight}px, 0)`
        else this.swiperElStyle.transform = `translate3d(-${this.swipeIndex * this.el.offsetWidth}px, 0, 0)`
    }
    // 设置轮播图容器的样式
    setSwiperElStyle() {
        for (let key in this.swiperElStyle)
            this.swiperEl.style[key] = this.swiperElStyle[key]
        
        // 尺寸动态变化所以放此处执行
        {
            this.swiperEl.style.width = `${this.el.offsetWidth * this.swiperItemEl.length}px` 
            // 设置滚动子容器宽度
            for (let item of this.swiperItemEl)
                item.style.width = `${this.el.offsetWidth}px`;
        }
    }
    startloop() {
        this.loopSTO = setTimeout(() => {
            this.swiperElStyle.transition = `transform ${this.duration}ms`
            this.loopNext()
            // this.loopPrev()
        }, this.autoplay)
    }
    // 跳转到指定下标图片 对外暴露
    setSwipeIndex(index) {
        clearTimeout(this.loopSTO)
        this.swipeIndex = index + 1
        this.loop()
        this.startloop()
    }

    // 轮播内容 滚动
    loop() {
        this.setSwiperElStyleTransform();
        this.setSwiperElStyle();
        this.setindicatorActiveClass();
    }

    loopNext() {
        clearTimeout(this.loopSTO)
        this.swipeIndex++
        this.loop()
        this.startloop()
        if (this.swiperItemEl.length - 1 == this.swipeIndex) {
            setTimeout(() => {
                this.swipeIndex = 1
                this.swiperElStyle.transition = "unset"
                this.loop()
            }, this.duration)
        }
    }
    loopPrev() {
        clearTimeout(this.loopSTO)
        this.swipeIndex--
        this.loop()
        this.startloop()
        if (0 == this.swipeIndex) {
            setTimeout(() => {
                this.swipeIndex = this.swiperItemEl.length - 2
                this.swiperElStyle.transition = "unset"
                this.loop()
            }, this.duration)
        }
    }
    touchstart(e) {
        let ev = e || window.event;
        let startX = ev.touches ? event.touches[0].clientX : event.screenX;
        let startY = ev.touches ? event.touches[0].clientY : event.screenY;
        this.touchCoordinate = { startX, startY, moveX: startX, moveY: startY }
        clearTimeout(this.loopSTO)
    }
    touchmove(e) {
        let ev = e || window.event;
        let moveX = ev.touches ? event.touches[0].clientX : event.screenX;
        let moveY = ev.touches ? event.touches[0].clientY : event.screenY;
        this.touchCoordinate = { ...this.touchCoordinate, moveX, moveY }
        this.swiperElStyle.transition = "unset"
        let moveW = this.touchCoordinate.moveX - this.touchCoordinate.startX
        this.swiperElStyle.transform = `translate3d(-${this.swipeIndex * this.el.offsetWidth - moveW}px, 0, 0)`
        this.setSwiperElStyle();
    }
    touchend(e) {
        let mz = this.touchCoordinate.moveX - this.touchCoordinate.startX;
        if (mz == 0) this.startloop()
        else if (mz < 0) {
            // 没超过指定距离 复位
            if (mz >= -(this.el.offsetWidth / 3)) {
                this.swipeIndex++
                this.swiperElStyle.transition = `transform 300ms`
            } else this.swiperElStyle.transition = `transform ${this.duration}ms`
            this[mz < -(this.el.offsetWidth / 3) ? 'loopNext' : 'loopPrev']()
        } else {
            // 没超过指定距离 复位
            if (mz < (this.el.offsetWidth / 3)) {
                this.swipeIndex--
                this.swiperElStyle.transition = `transform 300ms`
            } else this.swiperElStyle.transition = `transform ${this.duration}ms`
            this[mz < (this.el.offsetWidth / 3) ? 'loopNext' : 'loopPrev']()
        }
    }
    addEvent(obj, oEv, fn) {
        return obj.attachEvent ? obj.attachEvent('on' + oEv, fn) : obj.addEventListener(oEv, fn, false);
    }
    stop(){
        clearTimeout(this.loopSTO)
    }

}