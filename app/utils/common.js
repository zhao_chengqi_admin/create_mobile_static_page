const uuid = require("./uuid.js")
// 初始化dom节点
function initDomNode(node, pid = 0) {
    let nodes = []
    node.map(item => {
        if (item.pid == pid) {
            item.children = initDomNode(node, item.id)
            nodes.push(item)
        }
    })
    return nodes
}

// 把指定dom插入
function insertNode(iNode, targetNode, slot = "") {
    function get_slot(targetNode) {
        targetNode.map(item => {
            if (item.tag == "slot") {
                if (slot == "")
                    item.children = iNode
            }
            if (item.children) get_slot(item.children)
        })
    }
    get_slot(targetNode)
    return targetNode;
}

// 创建dom 节点
function render(node, option) {
    let html = ""
    let css_file = ""
    // let css_uuid = uuid.get_uuid();
    // console.log(pageTitle) 
    node.map(item => {
        if (item.tag) {
            if (item.tag == "slot") {
                // 插槽  html,css_file
                if (item.children) {
                    let child_obj = render(item.children, option)
                    html += child_obj.html
                    css_file += child_obj.css_file
                }
            } else {
                // let style =item.magic_style ? (item.magic_style).replace("{","").replace("}","") : ""
                // console.log(style) magic_css
                let magic_css = item.magic_css || ""
                let new_css = uuid.get_uuid()
                if (item.magic_style){
                    css_file += `.${new_css}{${ showJson( JSON.parse(item.magic_style) ) }}`
                    magic_css += " " +  new_css
                }
                // 双标签
                if (item.tag_type == 2) {
                    html += `<${item.tag} ${item.attr || ''}  class="${magic_css}"  ${item.magic_id ? 'id="' + item.magic_id + '"' : ""}>`;
                    // 有子标签
                    if (item.children) {
                        let child_obj = render(item.children, option)
                        html += child_obj.html
                        css_file += child_obj.css_file
                    }
                    if (item.text) html += item.text
                    if (item.tag == 'title') html += option.pageTitle || '' // 页面标题
                    html += `</${item.tag}>`;
                } else if (item.tag_type == 1) {
                    // 但标签
                    html += `<${item.tag} ${item.attr || ''}  class="${magic_css}"  ${item.magic_id ? 'id="' + item.magic_id + '"' : ""} />`;
                }

            }
        } else {
            // 当前无标签 从子标签获取dom 
            if (item.children) {
                let child_obj = render(item.children, option)
                html += child_obj.html
                css_file += child_obj.css_file
            }
        }
    })
    return { html, css_file }
}
// 创建dom 节点
function XMl_render(node, option) {
    let html = ""
    let css_file = ""
    // let css_uuid = uuid.get_uuid();
    // console.log(pageTitle) 
    node.map(item => {
        if (item.tag) {
            // 数据库中的插槽数据
            if (item.tag == "slot") {
                // 插槽  html,css_file
                if (item.children) {
                    let child_obj = XMl_render(item.children, option)
                    html += child_obj.html
                    css_file += child_obj.css_file
                }
            } else {
                // let style =item.magic_style ? (item.magic_style).replace("{","").replace("}","") : ""
                // console.log(style) magic_css  stylejson=
                let magic_css = item.magic_css || ""
                let new_css = uuid.get_uuid()
                if (item.magic_style){ 
                    css_file += `.${new_css}{${ showJson( JSON.parse(item.magic_style) ) }}`
                    // css_file += `.${new_css}${ (item.magic_style) }`
                    magic_css += " " +  new_css
                }
                // 双标签
                if (item.tag_type == 2) {
                    html += `<${item.tag} ${item.attr || ''} stylejson='${item.magic_style}' class="${magic_css}"  ${item.magic_id ? 'id="' + item.magic_id + '"' : ""}>`;
                    // 有子标签
                    if (item.children) {
                        let child_obj = XMl_render(item.children, option)
                        html += child_obj.html
                        css_file += child_obj.css_file
                    }
                    if (item.text) html += item.text
                    if (item.tag == 'title') html += option.pageTitle || '' // 页面标题
                    html += `</${item.tag}>`;
                } else if (item.tag_type == 1) {
                    // 但标签
                    html += `<${item.tag} ${item.attr || ''}  class="${magic_css}" stylejson='${item.magic_style}' ${item.magic_id ? 'id="' + item.magic_id + '"' : ""} />`;
                }

            }
        } else {
            // 当前无标签 从子标签获取dom 
            if (item.children) {
                let child_obj = XMl_render(item.children, option)
                html += child_obj.html
                css_file += child_obj.css_file
            }
        }
    })
    return { html, css_file }
}
function showJson(style){
    let s = []
    for(let i in style){
        s.push(i+':'+style[i]);
    }
    s = s.join(';')
    console.log(s)
    return  s
} 
module.exports = {
    initDomNode,
    insertNode,
    render,
    XMl_render,
    uuid
}