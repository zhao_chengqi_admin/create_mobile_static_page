import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    drop_menu_list: [
    //   {
    //   title: "Swipe轮播",
    //   image: "@/assets/image/icon_menu.png",
    //   type: "swipe"
    // },
    {
      title: "DIV标签",
      image: "@/assets/image/icon_menu.png",
      type: "DIVTAG"
    },
    {
      title: "IMG标签",
      image: "@/assets/image/icon_menu.png",
      type: "IMGTAG"
    },
    // {
    //   title: "H5标签",
    //   image: "@/assets/image/icon_menu.png",
    //   type: "H5TAG"
    // },
    // {
    //   title: "Cell单元格",
    //   image: "@/assets/image/icon_menu.png",
    //   type: "cell"
    // },{
    //   title: "导航栏",
    //   image: "@/assets/image/icon_menu.png",
    //   type: "swipe"
    // }
  ],
    uuid_list:[],
    javascript_option:{}, // js 脚本 执行所需要的参数
  },
  mutations: {
    SET_DATA(state,{ key,value }){
      state[key] = value
    }
  },
  actions: {
  },
  modules: {
  }
})