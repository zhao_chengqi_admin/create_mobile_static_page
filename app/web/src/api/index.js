import request from "@/utils/request"
// 获取指定页面dom  product?pageName=home
export const get_product_page = ({ pageName }) => request.get("/product",{ pageName })
export const page_save = (option) => request.post("/page/save",option)
export const upload_file = (option) => request.post("/upload_file", option , { 'Content-Type': 'multipart/form-data', })
// 获取页面列表
export const get_page_list = (option) => request.post("/page/list",option)
// 修改页面名字
export const savePageName = (option) => request.post("/page/savePageName",option)
export const createPage = (option) => request.post("/page/createPage",option)

