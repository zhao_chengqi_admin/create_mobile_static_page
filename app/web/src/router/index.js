import Vue from 'vue'
import VueRouter from 'vue-router'
import AsyncComponentHook from "@/utils/AsyncComponentHook.js"

Vue.use(VueRouter)

const routes = [
  {
    path: '/', redirect: { name: 'page_view_list' }
  },
  {
    path: '/',
    name: 'public',
    component: AsyncComponentHook("public/index"),
    children: [{
      path: '/index',
      name: 'index',
      component: AsyncComponentHook("home/index"),
      meta: {
        title: '首页',
      },
      query:{
        pageName: 'home'
      }
    },{
      path: '/page_view_list',
      name: 'page_view_list',
      component: AsyncComponentHook("page_view_list/index"),
      meta: {
        title: '页面列表',
      }
    }]
  },
]

const router = new VueRouter({
  // mode: 'history',
  base: "/",
  routes
})

router.beforeEach((to, from, next) => {
  document.title = to.meta.title
  // console.log(to,from)
  next()
})

export default router
