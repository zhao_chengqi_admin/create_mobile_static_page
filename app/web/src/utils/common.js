export function uuid(length) {
    var s = [];
    var hexDigits = "qwertyuiopasdfghjklzxcvbnm";
    // var hexDigits = "0123456789qwertyuiopasdfghjklzxcvbnm";
    for (var i = 0; i < length; i++) {
        s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
    }
    // s[14] = "4";  // bits 12-15 of the time_hi_and_version field to 0010
    // s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);  // bits 6-7 of the clock_seq_hi_and_reserved to 01
    // s[8] = s[13] = s[18] = s[23] = "-";
  
    var uuid = s.join("");
    return uuid;
}
export function getNodeJson(arr,filter){
    let json = [];
    for(let item of arr){
        let stylejson = item.getAttribute("stylejson") || '';
        // let style = stylejson && stylejson.replace(/,/g,";");
        let style = stylejson;
        let option = {
            tag : item.localName,
            class: item.getAttribute("class"),
            id: item.getAttribute("id"),
            style,
            children: [],
            attr:""
        }
        if(item.localName == "img")
            option.attr += `src="${item.getAttribute("src")}"` 
        if(item.children.length){
            option.children = getNodeJson(item.children,filter)
        }else{
            option.text = item.innerText
        }
        if(filter) json.push(filter(option))
        else json.push( option) 
    }
    return json
}
// 深度合并对象
export function deepObjectMerge(FirstOBJ, SecondOBJ) { // 深度合并对象 
    for (var key in SecondOBJ) {
        if(FirstOBJ[key]){ 
            if(Object.prototype.toString.call(FirstOBJ[key]) === "[object Object]"){
                FirstOBJ[key] = deepObjectMerge(FirstOBJ[key], SecondOBJ[key])
            }else if(Object.prototype.toString.call(FirstOBJ[key]) === "[object Array]"){
                FirstOBJ[key] = [ ...FirstOBJ[key],...SecondOBJ[key] ]
            }
        }else{
            FirstOBJ[key] = SecondOBJ[key]
        } 
    }
    return FirstOBJ;
}