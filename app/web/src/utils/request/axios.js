import axios from "axios"
import { base_url } from "./config"
const service = axios.create({
    // baseURL: process.env.API_ROOT,
    timeout: 120000
})
let host = process.env.NODE_ENV;
service.defaults.baseURL = base_url;

service.interceptors.request.use(
    config => {
        return config
    },
    error => {
        // Do something with request error
        console.log(error) // for debug
        Promise.reject(error)
    }
)

// request拦截器
service.interceptors.request.use(
    config => {
        // var token = sessionStorage.getItem('X-TOKEN')
        // if (token) {
        //     config.headers['X-Token'] = token // 让每个请求携带自定义token 请根据实际情况自行修改
        // }
        return config
    },
    error => {
        // Do something with request error
        console.log(error) // for debug
        Promise.reject(error)
    }
)

// respone拦截器
service.interceptors.response.use(
    function (response) {
        return response.data
    },
    function (error) {
        // debugger
        console.log('err' + error) // for debug 
        return Promise.reject(error)
    }
)

export default service