export default {
    swiper_view:{
        overflow: 'hidden',
        height: '200px',
        position: 'relative',
    },
    swiper:{
        display: 'flex',
        overflow: 'hidden',
        height: '100%', 
    },
    swiper_item:{
        display: 'flex',
        'align-items': 'center',
        'justify-content':'center',
        'flex-shrink': 0,
    },
    swiper_item_img:{
        width: '100%',
        height: '100%',
        display: 'block',
    },
    swiper_indicator:{
        position: 'absolute',
        "z-index":1,
        bottom: '10px',
        display: 'flex',
        'align-items': 'center',
        'justify-content': 'center',
        width: '100%',
        left: '0',
    },
    swiper_indicator_item:{
        width: '20px',
        margin: '0 3px',
        height: '3px',
    },
}