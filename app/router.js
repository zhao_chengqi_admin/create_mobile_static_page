'use strict';

/**
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
  const { router, controller } = app;
  router.get('/', controller.home.index);
  router.get('/product', controller.home.showDom);
  router.post('/page/save', controller.page.save);
  router.post('/page/savePageName', controller.page.savePageName);
  router.post('/page/list', controller.page.list);
  router.post('/page/createPage', controller.page.createPage);
  // 文件上传
  router.post('/upload_file', controller.file.upload_file); 
  router.get('/downloadPage', controller.file.downloadPage);  

};
